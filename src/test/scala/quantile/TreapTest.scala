package quantile

import org.scalatest.FunSuite

class TreapTest extends FunSuite {
  implicit val subTreeInfo = new SubTreapInfo[String, String] {
    override def recalc(root: Treap[String, String]): Unit = ()
  }
  implicit val priority = Ordering.by {str:String=>str.length}
  implicit val key = {str:String=>str}
  implicit val keyOrd = new Ordering[String]{
    override def compare(x: String, y: String): Int = x.compareToIgnoreCase(y)
  }

  type StringTreap = Treap[String, String]

  def CreateTreap(str:String, lhs:Option[StringTreap], rhs:Option[StringTreap]) = new StringTreap(str, lhs, rhs)(key, keyOrd, priority, subTreeInfo)

  test("test tree construction") {
    val t = CreateTreap("aa", None, None)
    val u = CreateTreap("b", None, None)
    val tu = Treap.meld(Some(t), Some(u))(key, keyOrd, priority, subTreeInfo)
    assert(tu.isDefined)
    val r = tu.get
    assert(r.value == t.value)
    assert(r.lhs.isEmpty)
    assert(r.rhs.isDefined)
    val q = r.rhs.get
    assert(q.value == u.value)
    assert(q.lhs.isEmpty)
    assert(q.rhs.isEmpty)
  }

  test("test division of treap") {

    /**
     *         bbb
     *      aa    bbb
     *                dd
     *              c
     */
    val t = CreateTreap("bbb", Some(CreateTreap("aa", None, None)), Some(CreateTreap("bbb", None, Some(CreateTreap("dd", Some(CreateTreap("c", None, None)), None)))))
    val (l,e,r) = t.split("bbb")
    assert(l.isDefined)
    assert(e.isDefined)
    assert(r.isDefined)
    val lRoot = l.get
    assert(lRoot.value == "aa")
    assert(lRoot.lhs.isEmpty)
    assert(lRoot.rhs.isEmpty)
    val eRoot = e.get
    assert(eRoot.value == "bbb")
    assert(eRoot.lhs.isEmpty)
    assert(eRoot.rhs.isDefined)
    val eNext = eRoot.rhs.get
    assert(eNext.lhs.isEmpty)
    assert(eNext.rhs.isEmpty)
    assert(eNext.value == "bbb")
    val rRoot = r.get
    assert(rRoot.value == "dd")
    assert(rRoot.lhs.isDefined)
    assert(rRoot.rhs.isEmpty)
    val rNext = rRoot.lhs.get
    assert(rNext.value == "c")
    assert(rNext.lhs.isEmpty)
    assert(rNext.rhs.isEmpty)
  }

}
