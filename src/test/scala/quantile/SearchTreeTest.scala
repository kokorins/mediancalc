package quantile

import org.scalatest.FunSuite

class SearchTreeTest extends FunSuite {

  implicit val subTreeInfo = new SubTreapInfo[String, String] {
    override def recalc(root: Treap[String, String]): Unit = ()
  }
  implicit val priority = Ordering.by {str:String=>str.length}
  implicit val key = {str:String=>str}
  implicit val keyOrd = new Ordering[String]{
    override def compare(x: String, y: String): Int = x.compareToIgnoreCase(y)
  }

  type StringTreap = Treap[String, String]

  def CreateTreap(str:String, lhs:Option[StringTreap], rhs:Option[StringTreap]) = new StringTreap(str, lhs, rhs)(key, keyOrd, priority, subTreeInfo)
  def CreateSearchTree() = new SearchTree[String, String]()(key, keyOrd, priority, subTreeInfo)

  test("testDeleteAll") {
    val expOrd = List("aa", "bbb", "bbb", "dd", "c")
    val st = expOrd.aggregate(CreateSearchTree())((st, str)=>st.add(str), (lhs, rhs)=>lhs.merge(rhs))
    val exp = st.deleteAll("bbb")
    val expRes = List("aa", "dd", "c")
    assert(exp.toList.size == expRes.size)
  }

  test("testMerge") {
    val leftElems = List("aa", "bbbb", "a", "a", "dd", "bb", "ccc", "d")
    val rightElems = List("bb", "dd", "aa", "a", "ccc", "cc", "d")
    val lTreap = leftElems.aggregate(CreateSearchTree())((st, str)=>st.add(str), (lhs, rhs)=>lhs.merge(rhs))
    val rTreap = rightElems.aggregate(CreateSearchTree())((st, str)=>st.add(str), (lhs, rhs)=>lhs.merge(rhs))
    val st = lTreap merge rTreap
    val expResult = leftElems ++ rightElems
    assert(st.toList.size == expResult.size)
    val pairs = expResult.sorted(keyOrd) zip st.toList
    assert(pairs forall{pair => pair._1 == pair._2})
  }

  test("testDelete") {
    val expOrd = List("aa", "bbb", "bb", "dd", "c")
    val st = expOrd.aggregate(CreateSearchTree())((st, str)=>st.add(str), (lhs, rhs)=>lhs.merge(rhs))
    val exp = st.delete(st.root.get.value)
    val expRes = List("aa", "bb", "dd", "c")
    assert(exp.toList.size == expRes.size)
  }

  test("testDeleteSubTree") {
    val expOrd = List("aa", "bbb", "bbb", "dd", "c")
    val st = expOrd.aggregate(CreateSearchTree())((st, str)=>st.add(str), (lhs, rhs)=>lhs.merge(rhs))
    val exp = st.deleteSubTree(st.root.get.rhs.get)
    val expRes = List("aa", "bbb")
    assert(exp.toList.size == expRes.size)
  }

  /**
   *    bbb
   * aa    bbb
   *          dd
   *         c
   */
  test("Test Tree construction and collection into a list") {
    val expOrd = List("aa", "bbb", "bbb", "dd", "c")
    val st = expOrd.aggregate(CreateSearchTree())((st, str)=>st.add(str), (lhs, rhs)=>lhs.merge(rhs))
    assert(st.toList.size == expOrd.size)
    val pairs = expOrd.sorted(keyOrd) zip st.toList
    assert(pairs forall{pair => pair._1 == pair._2})
  }
}
