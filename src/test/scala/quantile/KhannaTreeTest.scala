package quantile

import org.scalatest.FunSuite

import scala.util.Random

class KhannaTreeTest extends FunSuite {

  test("Insert and Merge without compress") {
    val exp = List(0.0, 1.0, 2.0, 3.0, 4.0)
    val kt = exp.aggregate(new KhannaTree[Double](0.01, 0.0, 0.0))({(kt, x)=>kt.insert(x)}, (lhs, rhs)=>lhs merge rhs)
    val med = kt.quantile(0.5).get
    assert(med == exp(2))
  }

  test("testCompress") {
    val exp = (0 to 10000).toList map {_.toDouble}
    val eps = 0.05
    val kt = exp.aggregate(new KhannaTree[Double](eps, 0.0, 0.0))({(kt, x)=>kt.insert(x)}, (lhs, rhs)=>lhs merge rhs)
    val med = kt.quantile(0.5).get
    assert(Math.abs(med- exp(exp.size/2))<=exp.size*eps)
  }

  test("testDelete") {
    val exp = (for(x<- 0 to 10) yield Random.nextInt(11)-5).toList
    val eps = 0.01
    val kt = exp.aggregate(new KhannaTree[Double](eps, 0.0, 0.0))({(kt, x)=>kt.insert(x)}, (lhs, rhs)=>lhs merge rhs)
    val med = kt.quantile(0.5).get
    assert(Math.abs(med)<=1e-3)

  }

}
