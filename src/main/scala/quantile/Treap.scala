package quantile
trait SubTreapInfo[Value, Key] {
  def recalc(root:Treap[Value, Key]):Unit
}

/**
 * Minimal Cartesian tree (aka Treap) implementation. All nodes are sorted according to to their keys and nodes with
 * higher priority are ancestors of nodes with lower priority.
 * Equal values are acceptable, they always go to the right subtree.
 * @param value the value stored in a current node
 * @param lhs left child node
 * @param rhs right child node
 * @param key function that allows to construct keys
 * @param keyOrd sorting function defined on a keys
 * @param priority sorting function defined on a values
 * @param subTreapInfo subtree node caching function
 * @tparam Value Acceptable types of values
 * @tparam Key Type of keys, could be the same as value
 */
case class Treap[Value,Key](value:Value, lhs:Option[Treap[Value, Key]], rhs:Option[Treap[Value, Key]])
                            (implicit key:(Value=>Key), keyOrd:Ordering[Key], priority:Ordering[Value], subTreapInfo:SubTreapInfo[Value, Key]) {
  type Node = Option[Treap[Value,Key]]
  type Trinomial = (Node, Node, Node)

  /**
   * Split treap into 3 parts such that first contains subtreap where all nodes are smaller than key, second - equals, third - greater.
   * Computational complexity is linear in depth (usually log(n), where n is number of nodes)
   * @param k the key that should be used for split
   * @return the tuple of a three subtreaps
   */
  def split(k: Key): Trinomial = {
    import Treap._
    val thisKey = key(value)
    if(thisKey == k) { // all nodes with same key is on the right
      val (l, e, r) = rhs.fold[Trinomial]((None, None, None))(_.split(k))
      val eqNodes = meld(e, Some(Treap(value, None, None)(key, keyOrd, priority, subTreapInfo)))
      eqNodes foreach {ens => subTreapInfo.recalc(ens)}
      (lhs, eqNodes, r)
    } else if (keyOrd.lt(thisKey,k)) {
      val (l, e, r) = rhs.fold[Trinomial]((None, None, None))(_.split(k))
      val leftNodes: Node = meld(meld(lhs, Some(Treap(value, None, None))), l)
      leftNodes foreach {lns => subTreapInfo.recalc(lns)}
      (leftNodes, e, r)
    } else {
      val (l, e, r) = lhs.fold[Trinomial]((None, None, None))(_.split(k))
      val rigthNodes = meld(r, meld(Some(Treap(value, None, None)(key, keyOrd, priority, subTreapInfo)), rhs))
      rigthNodes foreach {rns => subTreapInfo.recalc(rns)}
      (l, e, rigthNodes)
    }
  }

  /**
   * Create new treap with removed subtreap
   * Computation complexity is depth of a treap, usually log(n) where n is number of nodes in initial treap
   * @param node the node to be removed
   * @return new treap
   */
  def deleteSubTree(node:Treap[Value, Key]):Option[Treap[Value, Key]] = {
    if(this==node)
      None
    else if(keyOrd.lt(key(node.value), key(value)))
        Some(Treap(value, lhs flatMap {_.deleteSubTree(node)}, rhs))
    else
      Some(Treap(value, lhs, rhs flatMap {_.deleteSubTree(node)}))
  }
}

object Treap {
  /**
   * Combines two specially constructed treaps. The condition is next: left subtreap contains all the nodes with keys
   * less than right subtreap keys according to keyOrd
   * The meld procedure is linear in sum of depths of left and right treaps (usually log(n)),
   * where n is sum of number of nodes
   * @param lhs left treap
   * @param rhs right treap
   * @param key \see Treap
   * @param keyOrd \see Treap
   * @param priority \see Treap
   * @param subTreapInfo \see Treap
   * @tparam Value \see Treap
   * @tparam Key \see Treap
   * @return the single treap with nodes from both input treaps
   */
  def meld[Value,Key](lhs:Option[Treap[Value,Key]], rhs:Option[Treap[Value,Key]])
                     (implicit key:(Value=>Key), keyOrd:Ordering[Key], priority:Ordering[Value], subTreapInfo:SubTreapInfo[Value, Key]) : Option[Treap[Value,Key]] = {
    type Node = Option[Treap[Value,Key]]
    lhs.fold(rhs) { l=>
      rhs.fold(lhs) { r=>
        if(key(l.value)==key(r.value)) {
          val treap = Treap(l.value, l.lhs, meld(l.rhs, Some(r)))
          subTreapInfo.recalc(treap)
          Some(treap)
        } else {
          if(priority.compare(l.value, r.value)>0) {
            val treap = Treap(l.value, l.lhs, meld(l.rhs, Some(r)))
            subTreapInfo.recalc(treap)
            Some(treap)
          }
          else {
            val treap = Treap(r.value, meld(Some(l), r.lhs), r.rhs)
            subTreapInfo.recalc(treap)
            Some(treap)
          }
        }
      }
    }
  }
}
