package quantile

/**
 * Search tree implements balanced binary tree, with internal implementation as Treap
 * @param root the root node, empty only in case of empty tree
 * @param key \see Treap
 * @param keyOrd \see Treap
 * @param priority \see Treap
 * @param subTreapInfo \see Treap
 * @tparam Value \see Treap
 * @tparam Key \see Treap
 */
class SearchTree[Value, Key](val root:Option[Treap[Value, Key]])
                            (implicit key:(Value=>Key), keyOrd:Ordering[Key], priority:Ordering[Value], subTreapInfo:SubTreapInfo[Value, Key]) {
  /**
   * Construct empty tree
   * @param key \see Treap
   * @param keyOrd \see Treap
   * @param priority \see Treap
   * @param subTreapInfo \see Treap
   * @return empty SearchTree
   */
  def this()(implicit key:(Value=>Key), keyOrd:Ordering[Key], priority:Ordering[Value], subTreapInfo:SubTreapInfo[Value, Key]) = this(None)

  import Treap._

  /**
   * Add new value into a tree in about tree depth (usually log(n))
   * @param v the value to be added
   * @return new tree with additional element
   */
  def add(v:Value):SearchTree[Value, Key] = {
    new SearchTree(
      root.fold[Option[Treap[Value,Key]]](Some(Treap[Value,Key](v, None, None))) { rt=>
      import Treap._
      val (l, e, r) = rt.split(key(v))
      meld(meld(l, meld(e, Some(Treap(v, None, None)))),r)
    })
  }

  /**
   * Merges two trees into one in about n log n operations
   * @param that other tree
   * @return
   */
  def merge(that: SearchTree[Value, Key]):SearchTree[Value,Key] = {
    that.root.fold(this){ rhsRoot=>
      add(rhsRoot.value).merge(that.delete(rhsRoot.value))
    }
  }

  /**
   * Removes all the nodes with a given k in about log n
   * @param k the key to be removed
   * @return new tree without given key
   */
  def deleteAll(k:Key):SearchTree[Value, Key] = {
    new SearchTree[Value, Key](root flatMap { rt=>
      val (l, e, r) = rt.split(k)
      meld(l, r)
    })
  }

  /**
   * Removes exactly the value to be deleted.
   * Note that all the same values are deleted according to the equality function
   * It takes about log n operations
   * @param value the value to be deleted
   * @return new tree without given value
   */
  def delete(value:Value):SearchTree[Value, Key] = {
    def DeleteRightExact(eq:Option[Treap[Value, Key]], v:Value) : Option[Treap[Value, Key]] = {
      eq.fold(eq){e=>
        if(e.value != value)
          Some(Treap(e.value, e.lhs, DeleteRightExact(e.rhs, value)))
        else
          DeleteRightExact(e.rhs, value)
      }
    }
    new SearchTree[Value, Key](root flatMap { rt =>
      val (l, eqs, r) = rt.split(key(value))
      meld(l, meld(DeleteRightExact(eqs, value), r))
    })
  }

  /**
   * Removes all the nodes under given one in about log n operations
   * @param node to be removed
   * @return new tree with node removed
   */
  def deleteSubTree(node:Treap[Value, Key]):SearchTree[Value, Key] = {
    new SearchTree[Value, Key](root flatMap(_.deleteSubTree(node)))
  }

  /**
   * Reduce the tree into a given structure. goes exacty once through each node according the keyOrd ordering
   * @param init initial value of resulting structure
   * @param flatMap function that handles each value in a tree
   * @tparam B type of output value
   * @return the B constructed from all the values
   */
  def collect[B](init:B)(flatMap:((Value, B)=>B)):B = {
    var it = SearchTreeIterator.head(this)
    var res = init
    while(it.isDefined) {
      it = it flatMap { i =>
        res = flatMap(i.cur.value, res)
        i.next()
      }
    }
    res
  }

  /**
   * Constructs the list of all values in a tree in n operations for each of them the append of scala list is done
   * @return list of values sorted according to keyOrd
   */
  def toList = {
    collect[List[Value]](List()) { (v: Value, l: List[Value]) =>
      l :+ v
    }
  }
}
