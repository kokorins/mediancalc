package quantile

import scala.util.{Success, Failure, Try}


object QuantileImpl {
  case class QuantileSmImpl[S](values: List[S])(implicit ordering: Ordering[S]) extends Quantile[S] {
    def quant(x: Double): Try[S] = {
      val idx = Math.floor(x * (values.size - 1)).toInt
      Try(values.sorted.drop(idx).head)
    }

    def add(x: S) = new QuantileSmImpl(x :: values)

    override def merge(that: Quantile[S]): Try[Quantile[S]] = that match {
      case QuantileSmImpl(thatValues)=>Success(QuantileSmImpl(values ++ thatValues))
      case _ => Failure(new Exception("Incorrect merge type"))
    }
  }

  case class QuantileImpl[S](quantileTree: KhannaTree[S]) extends Quantile[S] {
    override def quant(x: Double): Try[S] = quantileTree.quantile(x)

    override def add(x: S) = new QuantileImpl(quantileTree.insert(x))

    override def merge(that: Quantile[S]): Try[Quantile[S]] = that match {
      case QuantileImpl(thatTree) => Success(QuantileImpl(quantileTree.merge(thatTree)))
      case _ => Failure(new Exception("Incorrect merge type"))
    }
  }

}