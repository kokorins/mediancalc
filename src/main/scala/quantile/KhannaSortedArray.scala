package quantile

import scala.util.{Success, Failure, Try}

case class KhannaSortedArray[S](values:List[KhannaNode[S]], n:Long, eps:Double)(ordering: Ordering[S]) extends Quantile[S] {
  override def quant(x: Double): Try[S] = {
    if(values.isEmpty)
      Failure(new Exception("Empty source"))
    else if(x<=0)
      values.headOption.fold[Try[S]](Failure(new Exception("Empty source"))){h=>
        Success(h.value)
      }
    else if(x>=1)
      values.lastOption.fold[Try[S]](Failure(new Exception("Empty source"))){l=>
        Success(l.value)
      }
    else {
      val r = Math.ceil(x*n)
      val curEps = eps*n
      val ranks = values.tail.scanLeft(values.head){(zero:KhannaNode[S], elem:KhannaNode[S])=>
        KhannaNode(elem.value, elem.groupSize+zero.groupSize, elem.generation)
      }
      val filtered = ranks filter{rank=>{
        r-rank.groupSize<=curEps && rank.groupSize+rank.generation>=curEps
      }}
      filtered.headOption.fold[Try[S]](Failure(new Exception("Invalid quantile"))){q=>
        Success(q.value)
      }
    }
  }

  override def add(x: S): Quantile[S] = {
    val generation = if(values.size<2 || ordering.lt(x, values.head.value) || ordering.gt(x, values.last.value)) 0L else Math.round(Math.floor(2*eps*(n+1)))
    insert(KhannaNode(x, 1, generation))
  }

  def insert(quantileNodes: List[KhannaNode[S]]):KhannaSortedArray[S] = quantileNodes match {
    case Nil => this
    case x::xs => insert(x).insert(xs)
  }

  def insert(quantileNode: KhannaNode[S]):KhannaSortedArray[S] = {
    type Quantiles = List[KhannaNode[S]]
    def insert(suffix:Quantiles, v:KhannaNode[S], prefix:Quantiles): Quantiles = suffix match {
      case Nil => prefix :+ v
      case x::xs => if(ordering.lt(v.value, x.value)) {
        (prefix :+ v) ++ suffix
      } else {
        insert(xs, v, prefix :+ x)
      }
    }
    def compact(values:Quantiles):Quantiles = {
      def merge(lhs:Option[KhannaNode[S]], rhs:KhannaNode[S]):KhannaNode[S] = {
        lhs.fold(rhs){l=>KhannaNode[S](l.value, l.groupSize+rhs.groupSize, l.generation)}
      }
      def capacity(node:KhannaNode[S]):Long = {
        node.groupSize+node.generation
      }
      def compact(suffix:Quantiles, prefix:Quantiles, group:Option[KhannaNode[S]], genCapacity: Long):Quantiles = suffix match {
        case Nil =>
          group.fold(prefix)(g=>prefix:+g)
        case x::xs => {
          val merged = merge(group, x)
          if(capacity(merged)<genCapacity)
            compact(xs, prefix, Some(merged), genCapacity)
          else
            compact(xs, group.fold(prefix){g=>prefix:+g}, Some(x), genCapacity)
        }
      }

      if(values.size<3)
        values
      else {
        val m = values.head
        val M = values.last
        val internalNodes = values.tail.init
        val newInternal = compact(internalNodes.reverse, List(), None, Math.round(Math.floor(2*eps*(n+1))))
        newInternal.reverse.+:(m).:+(M)
      }
    }
    new KhannaSortedArray[S](compact(insert(values, quantileNode, List())), n+1, eps)(ordering)
  }

  override def merge(that: Quantile[S]): Try[Quantile[S]] = that match {
    case KhannaSortedArray(thatValues, thatN, thatEps) => {
      if(Math.abs(eps-thatEps)>=Double.MinPositiveValue)
        Failure(new Exception("Different error limits"))
      else
        Success(insert(thatValues))
    }
    case _ => Failure(new Exception("No Such method"))
  }
}
