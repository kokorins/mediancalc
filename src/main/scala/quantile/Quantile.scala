package quantile

import scala.util.Try

trait Quantile[S] {
  def quant(x: Double): Try[S]
  def add(x: S): Quantile[S]
  def merge(that:Quantile[S]):Try[Quantile[S]]
}
