package quantile

/**
 * Left to right iterator according to keyOrd ordering
 * @param cur current node
 * @param parents known parents of the node
 * @tparam Value \see Treap
 * @tparam Key \see Treap
 */
class SearchTreeIterator[Value, Key](val cur:Treap[Value, Key], val parents:List[Treap[Value, Key]]) {
  import SearchTreeIterator._
  def next():Option[SearchTreeIterator[Value, Key]] = {
    def upUntil(pred:(Treap[Value, Key], Treap[Value, Key])=>Boolean)(c:Treap[Value, Key], parents:List[Treap[Value, Key]] = Nil):Option[SearchTreeIterator[Value, Key]] = parents match {
      case Nil => None
      case h :: t => if(pred(c, h)) {
        Some(SearchTreeIterator(h,t))
      } else {
        upUntil(pred)(h, t)
      }
    }
    cur.rhs.fold(upUntil{(n, p)=>
      p.rhs.fold(true){_ != n}
    }(cur, parents)){r=>Some(min(SearchTreeIterator(r, cur::parents)))}
  }
}

object SearchTreeIterator {
  def apply[Value, Key](cur:Treap[Value, Key], parents:List[Treap[Value, Key]]) = {
    new SearchTreeIterator[Value, Key](cur, parents)
  }
  def unapply[Value, Key](searchTreeIterator: SearchTreeIterator[Value, Key]) = Some(searchTreeIterator.cur, searchTreeIterator.parents)

  def min[Value, Key](cur:SearchTreeIterator[Value, Key]): SearchTreeIterator[Value, Key] = {
    cur.cur.lhs.fold(cur){l=>min(SearchTreeIterator(l, cur.cur::cur.parents))}
  }

  def max[Value, Key](cur:SearchTreeIterator[Value, Key]): SearchTreeIterator[Value, Key] = {
    cur.cur.rhs.fold(cur){r=>max(SearchTreeIterator(r, cur.cur::cur.parents))}
  }

  def head[Value, Key](searchTree: SearchTree[Value,Key]):Option[SearchTreeIterator[Value, Key]] = {
    searchTree.root map { rt=>
      min(SearchTreeIterator[Value,Key](rt, List()))
    }
  }

  def last[Value, Key](searchTree: SearchTree[Value, Key]):Option[SearchTreeIterator[Value,Key]] = {
    searchTree.root map { rt=>
      max(SearchTreeIterator[Value,Key](rt, List()))
    }
  }
}
