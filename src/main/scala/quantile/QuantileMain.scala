package quantile

import quantile.QuantileImpl.QuantileImpl

import scala.collection.immutable.TreeMap
import scala.util.{Random, Try}



object QuantileMain {
  def khannaArray() = {
    var quantiler:Quantile[Double] = new KhannaSortedArray[Double](List(), 0, 0.01)(Ordering[Double])
    val len = 5
    var map:TreeMap[Int, Long] = TreeMap((for(i<-0 until 5)yield i->0L): _*)
    for(i<- 0 until 10000) {
      val x = Random.nextInt(len)
      quantiler = quantiler.add(x.toDouble)
      //      map = map + (x -> (map(x)+1))
    }
    //    val z = map.scanLeft(0L)((zero:Long, v:(Int,Long))=>zero+v._2)
    //    val sum = z.last/2
    //    val sums = TreeMap((-1 until len) zip z : _*)
    //    val idx = sums filter {_._2<sum}
    val q = quantiler.quant(0.5)
    //    System.out.println(map)
    System.out.println(q)
  }
  def khannaTree() = {
    var quantiler:Quantile[Double] = new QuantileImpl[Double](new KhannaTree[Double](0.01, 0.0, 0.0))
    val len = 5
    var map:TreeMap[Int, Long] = TreeMap((for(i<-0 until 5)yield i->0L): _*)
    for(i<- 0 until 10000) {
      val x = Random.nextInt(len)
      quantiler = quantiler.add(x.toDouble)
      //      map = map + (x -> (map(x)+1))
    }
    //    val z = map.scanLeft(0L)((zero:Long, v:(Int,Long))=>zero+v._2)
    //    val sum = z.last/2
    //    val sums = TreeMap((-1 until len) zip z : _*)
    //    val idx = sums filter {_._2<sum}
    val q = quantiler.quant(0.5)
    //    System.out.println(map)
    System.out.println(q)
  }

  def main(args: Array[String]): Unit = {
    khannaTree()
  }
}
