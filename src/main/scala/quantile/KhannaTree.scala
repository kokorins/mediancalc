package quantile

import scala.util.{Failure, Success, Try}

case class KhannaNode[S](value: S, groupSize: Long, generation: Long)
case class KhannaNodeCached[S](value: S, groupSize: Long, generation: Long, var subTreeGroupSize:Long)

class NoGroupSizeInfo[S] extends SubTreapInfo[KhannaNode[S], S] {
  override def recalc(root: Treap[KhannaNode[S], S]): Unit = ()
}

class GroupSizeInfo[S] extends SubTreapInfo[KhannaNodeCached[S], S] {
  override def recalc(node:Treap[KhannaNodeCached[S], S]): Unit = {
    val leftGroup = node.lhs.fold(0L){l=>recalc(l);l.value.subTreeGroupSize}
    val rightGroup = node.rhs.fold(0L){r=>recalc(r);r.value.subTreeGroupSize}
    node.value.subTreeGroupSize = node.value.groupSize + leftGroup + rightGroup
  }
}

class KhannaTree[S](precision: Double, val treap: SearchTree[KhannaNodeCached[S], S], val min: S, val max: S)(implicit keyOrd: Ordering[S]) {
  type Value = KhannaNodeCached[S]
  type Key = S
  implicit object key extends (Value=>Key) {
    def apply(quantileNode: Value):Key = quantileNode.value
  }
  implicit object priority extends Ordering[Value] {
    override def compare(x: Value, y: Value): Int = Ordering[Long].compare(x.groupSize+x.generation, y.groupSize+y.generation)
  }
  implicit val subTreapInfo = new GroupSizeInfo[S]()

  def this(precision:Double, minL:S, maxL:S)(implicit keyOrd: Ordering[S]) = this(precision, new SearchTree[KhannaNodeCached[S], S]()
                                                                                 ({_.value}, keyOrd, Ordering.by(q=>q.groupSize+q.generation), new GroupSizeInfo[S]), minL, maxL)

  def merge(quantileTree: KhannaTree[S]) = {
    val t = treap.merge(quantileTree.treap)
    new KhannaTree[S](precision, t, SearchTreeIterator.head(t).fold(min){_.cur.value.value}, SearchTreeIterator.last(t).fold(max){it=>it.cur.value.value})
  }

  def quantile(x: Double): Try[S] = {
    val sz = treap.root.fold(0L) {_.value.subTreeGroupSize}
    val r = Math.round(x * sz)
    val l = precision * sz
    val ranks = treap.collect[List[KhannaNode[S]]](List()){(v,sum)=>
      if(sum.isEmpty)
        List(KhannaNode(v.value, v.groupSize, v.groupSize+v.generation))
      else {
        val last = sum.last
        sum :+ KhannaNode(v.value, last.groupSize + v.groupSize, last.groupSize + v.groupSize + v.generation)
      }
    }
    val quants = ranks filter {rank =>
       r - rank.groupSize<= l && rank.generation - r <= l
    }
    quants.headOption.fold[Try[S]](Failure(new Exception("Incorrect quantile"))) { h=>
      Success(h.value)
    }
  }

  def insert(x: S) = {
    val (locMin, locMax) = if(treap.root.isEmpty)(x, x) else (keyOrd.min(min,x), keyOrd.max(max,x))
    val sz = treap.root.fold(0L) {_.value.subTreeGroupSize}
    val d = if (keyOrd.lteq(x, min) || keyOrd.gteq(x, max)) 0 else Math.round(Math.floor(2 * precision * sz))
    val qt = new KhannaTree[S](precision, treap.add(KhannaNodeCached(x, 1, d, 1)), locMin, locMax)
    if ((sz + 1) % Math.round(1 / (2 * precision)) == 0)
      qt.compress()
    else
      qt
  }

  def compress():KhannaTree[S] = {
    val sz = treap.root.fold(0L) {_.value.subTreeGroupSize}
    new KhannaTree[S](precision, compress(treap.root, new SearchTree[Value, Key](None), Math.floor(2*precision*sz)), min, max)
  }

  private def compress(cio:Option[Treap[Value, Key]], treap:SearchTree[Value, Key], eps:Double):SearchTree[Value, Key] = {
    cio.fold(treap) { ci=>
      val gStar = ci.value.subTreeGroupSize
      if(gStar+ci.value.generation<=eps) {
        treap.add(KhannaNodeCached[S](ci.value.value, gStar, ci.value.generation, gStar))
      }
      else {
        compress(ci.rhs, compress(ci.lhs, treap.add(ci.value), eps), eps)
      }
    }
  }

  def delete(node: KhannaNodeCached[S]) = {
    new KhannaTree(precision, treap.delete(node), min, max)
  }

}